# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:51:55 2018

@author: Otso
"""
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 1000, 100)
y = np.log10(x+1)
plt.title('a graph')
plt.xlabel('X -label')
plt.ylabel('Y -label')
plt.grid()
plt.plot(x, y, label="Test title")
plt.show()
    

