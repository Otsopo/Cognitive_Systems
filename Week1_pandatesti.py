# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:57:40 2018

@author: Otso
"""

import pandas as pd
import numpy as np
text = 'https://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/processed.switzerland.data'
colnames = ['age','sex','cp','trestbps','chol','fbs','restecg','thalach','exang','oldpeak','slope','ca','thal','num']
data = pd.read_csv(text) #names = colnames,na_values = '?'
print(data)

df = pd.DataFrame(data)
df.columns = colnames
df = df.replace('?',np.NaN)
print(df)
print(df.max())

df.hist('age') #df['age'].hist()